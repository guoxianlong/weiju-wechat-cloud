// pages/test.js
import {
  verifyUser
} from "../../api/index"
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    wx.getSetting({
      success(res) {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称
          wx.getUserInfo({
            success: async function (res) {
              const data = await verifyUser(res.userInfo)
              if (data) {
                wx.setStorageSync('wjUser', data)
                wx.navigateBack({
                  delta:1
                })
              }
            }
          })
        }
      }
    })
  },

  // 跳转到主页
  bindgetuserinfo() {


  },




  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  getUserInfo: async function (e) {
    //获取用户的信息
    const data = await verifyUser(e.detail.userInfo)
    if (data) {
      wx.setStorageSync('wjUser', data)
      wx.navigateBack({
        delta:1
      })
    }
  }
})