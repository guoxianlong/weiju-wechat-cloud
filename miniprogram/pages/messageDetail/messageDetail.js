const app = getApp()

import {
  queryPageWjChatRecord,
  sendMsg
} from "../../api/index"

Page({

  /**
   * 页面的初始数据
   */
  data: {
    chatId: null, // 聊天室id
    msg: null,
    msgList: [],
    CustomBar: app.globalData.CustomBar,
    currentChatId: "" //当前滚动位置的id
  },

  messageListener: null,

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    let userInfo = wx.getStorageSync('wjUser')

    this.setData({
      chatId: options.chatId,
      userInfo: userInfo
    })


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

    this.initWatch()

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: async function () {

    this.queryPageWjChatRecord()

  },


  // 发送消息
  sendMsg: async function () {

    if (!this.data.msg) {
      return wx.showToast({
        title: '不可为空哦~',
        icon: "none"
      })
    }

    let data = {
      chatId: this.data.chatId,
      content: this.data.msg,
      type: 'text', // 先只支持文字
      headPortrait: this.data.userInfo.headPortrait
    }

    await sendMsg(data)


    this.setData({
      msg: null
    })

  },

  return (e) {
    wx.navigateBack({
      delta: 2
    })
  },


  // 消息输入监控
  sendInput(e) {

    console.log(e.detail.value)

    this.setData({
      msg: e.detail.value
    })

  },

  // 查询消息记录
  async queryPageWjChatRecord() {

    let data = {
      chatId: this.data.chatId,
      pageSize: 20,
      pageNO: 1
    }

    let result = await queryPageWjChatRecord(data);

    this.setData({
      msgList: result,
      // msgList: result.reverse(),
    })

  },

  async initWatch() {

    try {

      const db = this.db = wx.cloud.database()

      let chatId = this.data.chatId

      console.warn(`开始监听`, '聊天罗')
      this.messageListener = db.collection('chatRecord').orderBy('createTime', 'desc').where({
        chatId: chatId,
        delFlag: false,
        enabled: true
      }).limit(1).watch({
        onChange: this.onRealtimeMessageSnapshot.bind(this),
        onError: e => {
          wx.showToast({
            title: '连接失败',
            icon: 'none'
          })
          console.error(e)
        },
      })

    } catch (error) {

      wx.showToast({
        title: '初始化失败',
        icon: 'none'
      })
      console.error('初始化失败')

    }



  },

  onRealtimeMessageSnapshot(snapshot) {
    
    let msgList = this.data.msgList

    if (snapshot.type === 'init') {

    } else {

      for (const docChange of snapshot.docChanges) {
        switch (docChange.queueType) {
          case 'enqueue': {

            msgList.push(docChange.doc)

            break
          }
        }
      }
      this.setData({
        msgList: msgList
      })

    }
  },


  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {


  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {


  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})